package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import org.w3c.dom.Text;

public class VueGenerale extends Fragment {

    private String salle;
    private String poste;
    private String DISTANCIEL;
    private SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        this.poste = "";
        this.salle = this.DISTANCIEL;
        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);

        Spinner spSalle = (Spinner) getActivity().findViewById(R.id.spSalle);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.list_postes, R.layout.vue_generale);
        spSalle.setAdapter(adapter);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            TextView tvLogin = view.findViewById(R.id.tvLogin);
            model.setUsername(tvLogin.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        this.update(view);
        // TODO Q9
    }

    public void update(View view) {
        Spinner spPoste = (Spinner) view.findViewById(R.id.spPoste);
        if (salle.equals(DISTANCIEL)) {
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
            model.setLocalisation("Distanciel");
        } else {
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            Spinner spSalle = (Spinner) view.findViewById(R.id.spSalle);
            model.setLocalisation(spSalle.getSelectedItem().toString()+" : "+spPoste.getSelectedItem().toString());
        }
    }
    // TODO Q9
}